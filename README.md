# Timeline

An interactive timeline of a small slice of Mesopotamian history.

## Credits

This project was created by:

* Jannah Mandwee
* Amira Mandwee
* Alexey Nigin

It was our final project for the
[MIDEAST 341](https://www.coursicle.com/umich/courses/MIDEAST/341/) class at the
University of Michigan.

The project is available under the
[Creative Commons Attribution-NonCommercial license](https://creativecommons.org/licenses/by-nc/4.0/).

We have credited sources for information/images/etc. inline to the best of our
ability. If you have any comments or concerns about any content in this project,
please open an issue.
