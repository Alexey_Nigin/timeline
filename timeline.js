resize();

// global variables
var begDate;
var endDate;

var resultView = new Vue({
  el: '#app',
  // variables
  data: {
    periods: PERIODS,
    rulers: RULERS,
    events: EVENTS,
    filtered: EVENTS,
    landing_page: true,
    map: "images/maps/modern.jpeg"
  },
  // functions
  methods: {
    //SORT
    compare(a, b) {
      var A = a.year;
      var B = b.year;

      if(a.yearRange) {
        A = (a.yearStart + a.yearEnd)/2;
      }

      if(b.yearRange) {
        B = (b.yearStart + b.yearEnd)/2;
      }
    
      let comparison = 0;
      if (A <= B) {
        comparison = 1;
      } else if (A > B) {
        comparison = -1;
      }
      return comparison;
    },

    filterResults(startYear, endYear) {
      this.landing_page = false;
      this.filtered = [];
      // go through all events
      for(let i = 0; i < EVENTS.length; i++) {
        // calculate average year if yearRange == true
        if(EVENTS[i].yearRange) {
          let avg = Math.floor((EVENTS[i].yearStart + EVENTS[i].yearEnd)/2);
          if(avg <= startYear && avg >= endYear) {
            this.filtered.push(EVENTS[i]);
          }
        }
        // push event if fits in year range
        else {
          if(EVENTS[i].year <= startYear && EVENTS[i].year >= endYear) {
            this.filtered.push(EVENTS[i]);
          }
        } 
      }
      this.filtered.sort(this.compare);

      // Show the correct map
      let yearMid = (startYear + endYear) / 2;
      let minDist = Infinity;
      for (let p of PERIODS) {
        if (p.yearStart >= yearMid && yearMid >= p.yearEnd) {
          this.map = p.map;
          break;
        }
        let distStart = Math.abs(yearMid - p.yearStart);
        let distEnd = Math.abs(p.yearEnd - yearMid);
        let dist = Math.min(distStart, distEnd);
        if (dist < minDist) {
          minDist = dist;
          this.map = p.map;
        }
      }

      this.$forceUpdate();
    },

    sortPeriod(year, event) {
     if(4000 >= year && year > 3100) {
        return "Uruk";
      }
      else if(3100 >= year && year > 2900) {
        return "Jemdet Nasr";
      }
      else if(2900 >= year && year > 2725) {
        return "Early Dynastic I";
      }
      else if(2725 >= year && year > 2600) {
        return "Early Dynastic II";
      }
      else if(2600 >= year && year > 2475) {
        return "Early Dynastic IIIa";
      }
      else if(2475 >= year && year > 2350) {
        return "Early Dynastic IIIb";
      }
      else if (2350 >= year && year > 2150) {
        return "Akkadian";
      }
      else if (2112 >= year && year > 2004) {
        return "Ur III";
      }
      else if (1894 >= year && year > 1595) {
        return "Old Babylonian";
      }
      else if(1595 >= year && year > 1155) {
        return "Kassite";
      }
      else if(1155 >= year && year > 911) {
        return "Middle Assyrian";
      }
      else if(911 >= year && year > 626) {
        return "Neo-Assyrian";
      }
      else if(626 >= year && year > 550) {
        return "Neo-Babylonian";
      }
      else {
        return "No information available";
      }
    },

    getBorder(result) {
      for (type of EVENT_TYPES) {
        if (result.type === type.name) {
          return "border-color: #" + colorToCSS(type.color);
        }
      }
      console.log("type not found");
      return "";
    }

  }
})

function colorToCSS(color) {
  let s = color.toString(16);
  while (s.length < 6) s = "0" + s;
  return s;
}

//// Let Pixi begin ////

// Generic stuff

WebFont.load({
  google: {
    families: ['Fondamento']
  },
  active: function() {
    createTimeline();
  }
});

function createTimeline() {
  T = new PIXI.Application({
    resizeTo: $("#timeline")[0],
    antialias: true
  });
  $("#timeline").append(T.view);
  T.renderer.backgroundColor = 0xf3eac0;
  T.objects = {};
  createRulers();
  createEvents();
  createPeriods();
  createDates();
  createSelection();
  createListeners();
}

function yearToX(year) {
  let x = (YEAR_START - year) / (YEAR_START - YEAR_END) * T.screen.width;
  return Math.round(x);
}

function xToYear(x) {
  let year = YEAR_START - x * (YEAR_START - YEAR_END) / T.screen.width;
  return Math.round(year);
}

// Rulers

function createRulers() {
  T.objects.rulers = new PIXI.Container();
  T.objects.rulers.position.set(0, 0);
  for (let r of RULERS) {
    let ruler = new PIXI.Graphics();
    ruler.yearStart = r.yearStart;
    ruler.yearEnd = r.yearEnd;
    T.objects.rulers.addChild(ruler);
  }
  updateRulers();
  T.stage.addChild(T.objects.rulers);
}

function updateRulers() {
  for (let ruler of T.objects.rulers.children) {
    ruler.clear();
    ruler.beginFill(0xffff00);
    let xStart = yearToX(ruler.yearStart);
    let xEnd = yearToX(ruler.yearEnd);
    ruler.drawRect(xStart, 0, xEnd - xStart, 80);
    ruler.endFill();
  }
}

// Events

function createEvents() {
  T.objects.events = new PIXI.Container();
  T.objects.events.position.set(0, 0);
  for (let e of EVENTS) {
    let event = new PIXI.Graphics();
    event.year = e.yearRange ? ((e.yearStart + e.yearEnd) / 2) : e.year;
    event.position.y = 5;
    typeFound = false;
    for (type of EVENT_TYPES) {
      event.position.y += 10;
      if (e.type === type.name) {
        event.color = type.color;
        typeFound = true;
        break;
      }
    }
    if (!typeFound) console.log("event type not found");
    T.objects.events.addChild(event);
  }
  updateEvents();
  T.stage.addChild(T.objects.events);
}

function updateEvents() {
  for (let event of T.objects.events.children) {
    event.clear();
    event.beginFill(event.color);
    event.position.x = yearToX(event.year);
    event.drawCircle(0, 0, 3);
    event.endFill();
  }
}

// Periods

function createPeriods() {
  T.objects.periods = new PIXI.Container();
  T.objects.periods.position.set(0, 80);
  for (let p of PERIODS) {
    let period = new PIXI.Graphics();
    period.yearStart = p.yearStart;
    period.yearEnd = p.yearEnd;
    period.color = p.color;
    T.objects.periods.addChild(period);
  }
  updatePeriods();
  T.stage.addChild(T.objects.periods);
}

function updatePeriods() {
  for (let period of T.objects.periods.children) {
    period.clear();
    period.beginFill(period.color);
    let xStart = yearToX(period.yearStart);
    let xEnd = yearToX(period.yearEnd);
    period.drawRect(xStart, 0, xEnd - xStart, 10);
    period.endFill();
  }
}

// Dates

function createDates() {
  T.objects.dates = new PIXI.Container();
  T.objects.dates.position.set(0, 90);
  for (let y = YEAR_START - YEAR_START % 100; y >= YEAR_END; y -= 100) {
    let tick = new PIXI.Graphics();
    tick.year = y;
    tick.beginFill(0x000000);
    let h = 5;
    if (y % 500 === 0) h += 5;
    if (y % 1000 === 0) h += 5;
    tick.drawRect(-1, 0, 2, h);
    tick.endFill();
    tick.position.y = 0;
    if (y % 500 === 0) {
      let label = new PIXI.Text(
        String(y) + " BC",
        new PIXI.TextStyle({
          fontFamily: "'Fondamento', cursive",
          fontSize: 12,
          fill: "#000000"
        })
      );
      label.anchor.set(0, 1);
      label.position.y = 30;
      tick.addChild(label);
    }
    T.objects.dates.addChild(tick);
  }
  updateDates();
  T.stage.addChild(T.objects.dates);
}

function updateDates() {
  for (let tick of T.objects.dates.children) {
    tick.position.x = yearToX(tick.year);
    if (tick.children.length === 1) {
      let label = tick.children[0];
      let x_opt = - label.width / 2;
      let x_min = 5 - tick.position.x;
      let x_max = T.screen.width - label.width - 5 - tick.position.x;
      label.position.x = Math.round(Math.min(Math.max(x_opt, x_min), x_max));
    }
  }
}

// Selection

function createSelection() {
  T.objects.selection = new PIXI.Graphics();
  T.objects.selection.yearStart = 2300;
  T.objects.selection.yearEnd = 2200;
  updateSelection();
  T.stage.addChild(T.objects.selection);
}

function updateSelection() {
  T.objects.selection.clear();
  T.objects.selection.lineStyle(2, 0x0000ff, 1, 0);
  let xStart = yearToX(T.objects.selection.yearStart);
  let xEnd = yearToX(T.objects.selection.yearEnd);
  T.objects.selection.drawRoundedRect(xStart, 0, xEnd - xStart, 125, 5);
}

function resize() {
  let occupied = $("header").outerHeight() + $("#timeline").outerHeight() + 120;
  $(".info-display").height($(window).outerHeight() - occupied);
  if (typeof(T) === "undefined") return;
  updateRulers();
  updateEvents();
  updatePeriods();
  updateDates();
  updateSelection();
  updateHitArea();
}

function createListeners() {
  T.stage.interactive = true;
  T.stage.on("pointerdown", startTrack);
  T.stage.on("pointerup", endTrack);
  T.stage.on("pointerupoutside", endTrack);
  T.stage.on("pointermove", changeSelection);
  updateHitArea();
  $(window).resize(resize);
}

function updateHitArea() {
  T.stage.hitArea = new PIXI.Rectangle(0, 0, T.screen.width, T.screen.height);
}

let track = false;

function startTrack(event) {
  track = true;
  changeSelection(event);
}

function endTrack() {
  track = false;
}

function changeSelection(event) {
  if (!track) return;
  let yearMax = YEAR_START - 50;
  let yearMin = YEAR_END + 50;
  let yearMouse = xToYear(event.data.global.x);
  let yearCenter = Math.min(Math.max(yearMouse, yearMin), yearMax);
  T.objects.selection.yearStart = yearCenter + 50;
  T.objects.selection.yearEnd = yearCenter - 50;
  for (r of RULERS) {
    if (r.yearStart >= yearCenter && yearCenter >= r.yearEnd) {
      T.objects.selection.yearStart = r.yearStart;
      T.objects.selection.yearEnd = r.yearEnd;
      break;
    }
  }
  updateSelection();
  let yearStart = T.objects.selection.yearStart;
  let yearEnd = T.objects.selection.yearEnd;
  resultView.filterResults(yearStart, yearEnd);
}
