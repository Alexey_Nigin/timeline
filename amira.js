const EVENTS = [
 {
    type: "Tablets",
    yearRange: false,
    year: 3200,
    isEvent: true,
    location: "Uruk",
    name: "Development of Cuneiform Writing System",
    description: "The cuneiform writing system was initially created for recording transactions and was written on clay tablets. Later, scribes would chisel cuneiform into stone as well.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.archaeology.org/issues/213-1605/features/4326-cuneiform-the-world-s-oldest-writing",
        trusted: true
      }
    ]
  }, 
  {
    type: "stone",
    yearRange: true,
    yearStart: 2900,
    yearEnd: 2600,
    isEvent: false,
    location: "Square Temple - Eshnunna (modern Tell Asmar in Iraq)",
    name: "Stone Statue of Standing Male Worshiper",
    description: "This standing figure, with clasped hands and a wide-eyed gaze, is a worshiper. It was placed in the 'Square Temple' at Tell Asmar, perhaps dedicated to the god Abu, in order to pray perpetually on behalf of the person it represented. For humans equally were considered to be physically present in their statues. Similar statues were sometimes inscribed with the names of rulers and their families.",
    image: "",
    imageSource: "https://www.metmuseum.org/art/collection/search/323735", 
    references: [
      {
        url: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/323735/711771/main-image",
        trusted: true
      }
    ]
  },
  {
    type: "stone",
    yearRange: true,
    yearStart: 2600,
    yearEnd: 2500,
    isEvent: false,
    location: "Temple of Inanna/Ishtar - Nippur",
    name: "Stone Statue of Standing Female Worshiper",
    description: "This standing figure with clasped hands was buried within the temple furniture, suggesting that temple offerings and equipment remained sacred even when no longer in use.",
    image: "",
    imageSource: "https://www.metmuseum.org/art/collection/search/329080", 
    references: [
      {
        url: "https://www.metmuseum.org/art/collection/search/329080",
        trusted: true
      }
    ]
  },
  {
    type: "stone",
    yearRange: true,
    yearStart: 1243,
    yearEndyo: 1207,
    isEvent: false,
    name: "Inscribed Tablet",
    description: "This tablet commemorates this restoration of the temple of the Goddess Ishtar in the capital city of Assur as well as notes the good deeds and conquests of King Tukulti-Ninurta I. The inscription concludes with a curse on anyone would would attempt to remove the king's name from the tablet.",
    image: "",
    imageSource: "https://www.themorgan.org/exhibitions/written-in-stone", 
    references: [
      {
        url: "https://www.themorgan.org/exhibitions/written-in-stone",
        trusted: true
      }
    ]
  },
  {
    type: "wood",
    yearRange: false,
    year: 6000,
    isEvent: true,
    name: "Creation of Plow",
    description: "The first developments into society and civilization occurred when humans transitioned from hunter-gatherers to farmers. They planted seeds by developing a plow out of a stone blade and attaching it to a wooden shaft, having it pulled by oxen to dig holes for the seeds to be planted in.",
    image: "",
    imageSource: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html", 
    references: [
      {
        url: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html",
        trusted: false
      }
    ]
  }, 
  {
    type: "wood",
    yearRange: false,
    year: 2350,
    isEvent: true,
    location: "Assyria",
    name: "Creation of Composite Bow",
    description: "The composite bow was created by gluing together layers of wood from different trees with different elasticity and strength properies, using glues derived from animal bone and sinew. This bow design was so powerful and resilient that it persisted for centuries.",
    image: "images/08bow.jpg",
    imageSource: "http://www.mesopotamia.co.uk/warfare/explore/images/08bow.jpg",
    references: [
      {
        url: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html",
        trusted: false
      }
    ]
  },  
  {
    type: "wood",
    yearRange: false,
    year: 2300,
    isEvent: true,
    name: "Creation of Seed Plow",
    description: "Sumerians revolutionized agriculture by creating the seed plow, in which a funnel was attached to the plow to deposit the seeds automatically after the plow dug the hole in the soil.",
    image: "",
    imageSource: "https://sites.google.com/a/brvgs.k12.va.us/wh-15-sem-1-mesopotamia-go/seeder-plow", 
    references: [
      {
        url: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html",
        trusted: false
      }
    ]
  },
  {
    type: "wood",
    yearRange: false,
    year: 900,
    isEvent: true,
    name: "Use of Wells",
    description: "By 900 BCE, wood was used in the building of wells. As wells were created to retrieve water, some wells were made large and deep. While the steps were usually made from stone or the bedrock from the soil, the walls would sometimes be lined with wood (or stone or plaster) to hold it up. Water would be retrieved by a bucket and rope, somtimes accompanied with a wooden crank.",
    image: "",
    imageSource: "", 
    references: [
      {
       //The source is the TANE, page 156. Not sure how to cite a URL here",
        trusted: true
      }
    ]
  },
  {
    type: "cooking",
    yearRange: false,
    year: 9000,
    isEvent: true,
    name: "Animal Domestication",
    description: "Evidence for the cultivation of animal domestication first appears in Mesopotamia and spreads steadily to Egypt, Canaan, and Turkey.",
    image: "",
    imageSource: "", 
    references: [
      {
        // The source is TANE, page 255,
        trusted: true
      }
    ]
  },
  {
    type: "cooking",
    yearRange: false,
    year: 4000,
    isEvent: true,
    name: "Taboon Ovens",
    description: "Taboon/tannur bread was baked from flour in taboon/tannur ovens. These ovens were shaped with beehive-like coils in the form of a cone and constructed with clay, reed, goat hair, pebbles, or sheep's wool. There was a wide hole at the top for ventilation and for fuel and a small one at the base.",
    image: "",
    imageSource: "", 
    references: [
      {
        // The source is TANE, page 259,
        trusted: true
      }
    ]
  },
  {
    type: "cooking",
    yearRange: false,
    year: 3500,
    isEvent: true,
    name: "Earliest Noted Discovery of Beer",
    description: "The earliest mention of beer in Mesopotamia is in the song 'Hymn to Ninkasi' or the goddess of brewing. Written down around 1800 BCE, the hym is no doubt much older as evidenced by the techniques detailed in the poem which scholars have determined were actually in use long before the hymn was written. Bread and beer went hand-in-hand since it is from bread dough that beer is fermented. Women were the primary brew masters, and their craft was even mentioned in Hammurabi's code.",
    image: "",
    imageSource: "", 
    references: [
      {
        //TANE pages 260 and 261 were also references
        url: "https://www.ancient.eu/article/222/the-hymn-to-ninkasi-goddess-of-beer/",
        trusted: true
      }
    ]
  },
  {
    type: "cooking",
    yearRange: false,
    year: 1700,
    isEvent: true,
    name: "Cooling and Refrigeration First Utilized",
    description: "The ruler Zimri-lin of Mari built ice houses, 'bit shuripim', near his capital.",
    image: "",
    imageSource: "", 
    references: [
      {
        //TANE page 256 url: "",
        trusted: true
      }
    ]
  },
  {
    type: "architecture",
    yearRange: false,
    year: 3000,
    isEvent: true,
    name: "Mesopotamian Kings Begin Building Ziggurats",
    description: "Ziggurats were ancient buildings built high up with many stairs. They were meant to praise the Gods, which is why they were so high up. Ziggurats were part of a temple complex, a set of buildings devoted to the care of the gods and to all the businesses of the temple. The temple complex was one of the economic centers of the city. Large temples employed hundreds or even thousands of people, from priests and priestesses to humble shepherds, carpenters and weavers.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.historyonthenet.com/ziggurats-and-temples-in-ancient-mesopotamia",
        trusted: false
      }
    ]
  },
  {
    type: "architecture",
    yearRange: false,
    year: 2000,
    isEvent: true,
    location: "Ur, Sumer (Present-day Nasiriyah, Iraq)",
    name: "The Great Ziggurat of Ur",
    description: "Construction began by Sumerian King Ur-Nammu during the Third Dynasty of Ur and was finished within the 21st century BCE by King Shugli. During King Shugli’s rule, the city of Ur grew to be the capital of a state controlling much of Mesopotamia, meaning that this ziggurat was an important center for Mesopotamia as a whole. During the Neo-Babylonian era, the ziggurat had deteriorated to just the base level. It was entirely rebuilt by King Nabonidus in the 6th century B.C.",
    image: "",
    imageSource: "https://www.ancient.eu/image/197/great-ziggurat-of-ur/", 
    references: [
      {
        url: "https://www.historyonthenet.com/ziggurats-and-temples-in-ancient-mesopotamia",
        trusted: false
      }
    ]
  },
  {
    type: "water",
    yearRange: false,
    year: 800,
    isEvent: true,
    location: "Assyria",
    name: "Canal Systems Constructed",
    description: "The first sophisticated long-distance canal systems were constructed in the Assyrian Empire. The purpose of aqueducts were “to transport water from one place to another, achieving a regular and controlled water supply to a place that would not otherwise have received sufficient water to meet basic needs such as irrigation of food crops and drinking fountains. Being able to control water allowed agriculture to flourish and allowed cities to expand to beyond just along water sources.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.ancient.eu/aqueduct/",
        trusted: true
      }
    ]
  },
  {
    type: "government",
    yearRange: true,
    yearStart: 1792,
    yearEnd: 1750,
    isEvent: false,
    location: "Babylon",
    name: "Rule of King Hammurabi",
    description: "Hammurabi was the sixth king of the Amorite First Dynasty of Babylon, following the throne from his father Sin-Muballit. He was known for expanding the kingdom of Babylon with the intentions of conquering all of Mesopotamia. At that time of his rise to power, Kingdom of Babylon consisted of the cities of Babylon, Kish, Sippar, and Borsippa. By 1750 BCE, he had control of the entire region of Babylon./nHe was also know for providing for his people. Hammurabi was referred to as 'bani matim', or 'builder of the land' for commissioning the construction of buildings and canals to develop the region. He also established efforts for food distribtuion, land and arhchitectural beautification, and legal systems./nFinally, he is most remembered for his code of 282 laws. They were written with the goal of pleasing the gods. They were very strict and absolute and provided a number of crimes and their applicable punishents, one of the most notable examples being 'an eye for an eye'. The laws were carved into stone to emphasize their permanence and were displayed publicly so that no citizen or slave could claim he was not aware of a certain law or infraction.",
    image: "",
    imageSource: "https://www.ancient.eu/article/68/hammurabis-code-babylonian-law-set-in-stone/", 
    references: [
      {
        url: "https://www.ancient.eu/hammurabi/",
        trusted: true
      }
    ]
  },
  {
    type: "government",
    yearRange: false,
    year: 2500,
    isEvent: true,
    location: "Uruk, Sumer",
    name: "Rule of King Gilgamesh",
    description: "Gilgamesh was the 5th king of Uruk in Sumer. His father is said to have been Priest-King Lugalbanda and his mother Goddess Ninsun. As such, Gilgamesh was a demi-god who possessed super-human strength and lived an exceptionally long life, with records marking him to live 126 years./nHe is best remembered for the Epic of Gilgamesh, which was written between 2150-1400 BCE./nHis influence was so profound that myths of his divine status grew up around his deeds and finally culminated in the tales found in The Epic of Gilgamesh. Later Mesopotamian kings would invoke his name and associate his lineage with their own.",
    image: "",
    imageSource: "https://www.ancient.eu/gilgamesh/", 
    references: [
      {
        url: "https://www.ancient.eu/gilgamesh/",
        trusted: true
      }
    ]
  },
  {
    type: "government",
    yearRange: true,
    yearStart: 668,
    yearEnd: 627,
    isEvent: false,
    location: "Assyria",
    name: "Rule of King Ashurbanipal",
    description: "Ashurbanipal was the last of the great kings of Assyria, living from 668-627 BCE. He was the son of King Esarhaddon of the Neo-Assyrian Empire./nHe is known for achieving the greatest territorial expansion of the Assyrian Empire, eventually capturing Babylonia, Persia, Syria, and briefly Egypt. He was popular among his citizens, but known to be cruel to those whom he defeated./nHe is also known for his grand library in Nineveh, which housed over 30,000 clay tablets, including the Babylonian Epic of Creation and the Epic of Gilgamesh.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.ancient.eu/Ashurbanipal/",
        trusted: true
      }
    ]
  },
  {
    type: "government",
    yearRange: false,
    year: 4000,
    isEvent: true,
    name: "Rule of Sumerian Empire",
    description: "The civilization of Sumer was created around 4000 BCE. The communities were independent city-states structured around a temple and ruled by a priesthood but had both secular and spiritual leaders. The temples were both religious and administrative centers for the city-states.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "government",
    yearRange: true,
    yearStart: 2300,
    yearEnd: 2100,
    isEvent: false,
    name: "Rule of Akkadian Empire",
    description: "The Akkadian empire developed during the third millennia in the north of Mesopotamia (while the Sumerians were still ruling in the south). Akkadians created a new form of government where kings had an absolute power and this title was hereditary. These rulers referred to themselves as Lord of the Four Quarters (of the Earth) and eventually elevated themselves to a divine status.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "government",
    yearRange: true,
    yearStart: 2000,
    yearEnd: 1600,
    isEvent: false,
    name: "Rule of Amorite Empire",
    description: "The Amorites ruled between roughly 2000 and 1600 BCE. They adopted the tyrannical form of government that was established by the Akkadians, and they established the first Babylonian Dynasty. With accomplishments like Hammurabi’s Code, they maintained a centralized administration and created legislative and jurisdictional authority",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "government",
    yearRange: true,
    yearStart: 2500,
    yearEnd: 612,
    isEvent: false,
    name: "Rule of Assyrian Empire",
    description: "The Assyrians ruled roughly between 2500 and 612 BCE. This was a greatly militarized society thought to be the result of its struggles with independence and expansionist police. Kings had absolute power and collected heavy taxes. Assyrian leaders were known for mass deportations of their defeated or rebellious peoples.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "trade",
    yearRange: false,
    year: 3000,
    isEvent: true,
    name: "Establishment of Long-distance Trade",
    description: "A variety of goods were traded between cities such as textiles, pottery, leather goods, jewelry, spices, gains, and metals. Transportation was made easier with the invention of the wheel and sail, so items previously delivered by ox carts began being transported by water on riverboats./nAround 3000 BCE, Mesopotamian cities established trade all up and down the Tigris and Euphrates rivers and into Anatolia, today’s Turkey. Other overland trade routes went east over the Zagros Mountains into present-day Iran and Afghanistan. By 2000 BCE, trade went in all directions. The creation of writing and math systems allowed for the organization and documentation of these economic endeavors, especially as they became more complicated and wide-spread.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.historyonthenet.com/mesopotamian-merchants-and-traders",
        trusted: false
      }
    ]
  },
  {
    type: "education",
    yearRange: false,
    year: 2000,
    isEvent: true,
    location: "Sumer",
    name: "Establishment of Scribe Schools",
    description: "During around 2000 BCE, scribal schools appeared in Ur, Nippur, and Sippar to teach students mathematics and writing, particularly as the system of cuneiform writing developed. All students learned reading, writing, math, and history, literacy, numeracy, geography, zoology, botany, astronomy, engineering, medicine, and architecture. Students would use clay tablets to practice writing; they would rewrite sentences until they were no errors.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.historyonthenet.com/mesopotamian-education-and-schools",
        trusted: false
      }
    ]
  },
]