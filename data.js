let T;

YEAR_START = 4050;
YEAR_END   = 530;

class Period {
  constructor(name, yearStart, yearEnd, color, map) {
    this.name = name;
    this.yearStart = yearStart;
    this.yearEnd = yearEnd;
    this.color = color;
    this.map = "images/maps/" + map;
  }
};

class EventType {
  constructor(name, color) {
    this.name = name;
    this.color = color;
  }
};

const PERIODS = [
  new Period("Uruk",                4000, 3100, 0x800000, "uruk.png"),
  new Period("Jemdet Nasr",         3100, 2900, 0xff0000, "jemdet-nasr.png"),
  new Period("Early Dynastic I",    2900, 2725, 0x606000, "early-dynastic.png"),
  new Period("Early Dynastic II",   2725, 2600, 0x909000, "early-dynastic.png"),
  new Period("Early Dynastic IIIa", 2600, 2475, 0xc0c000, "early-dynastic.png"),
  new Period("Early Dynastic IIIb", 2475, 2350, 0xffff00, "early-dynastic.png"),
  new Period("Akkadian",            2350, 2150, 0x008000, "akkadian.png"),
  new Period("Ur III",              2112, 2004, 0x00ff00, "ur-iii.png"),
  new Period("Old Babylonian",      1894, 1595, 0x008080, "old-babylonian.png"),
  new Period("Kassite",             1595, 1155, 0x000080, "kassite.png"),
  new Period("Middle Assyrian",     1155,  911, 0x8000ff, "middle-assyrian.png"),
  new Period("Neo-Assyrian",         911,  626, 0xff00ff, "neo-assyrian.png"),
  new Period("Neo-Babylonian",       626,  539, 0x808080, "neo-babylonian.png")

];

const EVENT_TYPES = [
  new EventType("Math",              0xff0000),
  new EventType("Writing",           0x008000),
  new EventType("Natural Resources", 0x808080),
  new EventType("Cooking",           0xffa500),
  new EventType("Civil Engineering", 0x00ced1),
  new EventType("Government",        0x800080) // Could potentially replace idea of spanning timeline by ruler? -Jannah
];

const EVENTS = [
  {
    type: "Cooking",
    yearRange: false,
    year: 4000,
    isEvent: false,
    name: "Taboon Ovens",
    description: "Taboon/tannur bread was baked from flour in taboon/tannur ovens. These ovens were shaped with beehive-like coils in the form of a cone and constructed with clay, reed, goat hair, pebbles, or sheep's wool. There was a wide hole at the top for ventilation and for fuel and a small one at the base.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://books.google.com/books?id=A3xqDwAAQBAJ&lpg=PP1&pg=PT473#v=onepage&q&f=false",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: false,
    year: 4000,
    isEvent: false,
    name: "Rule of Sumerian Empire",
    description: "The civilization of Sumer was created around 4000 BCE. The communities were independent city-states structured around a temple and ruled by a priesthood but had both secular and spiritual leaders. The temples were both religious and administrative centers for the city-states.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 3500,
    isEvent: false,
    name: "Kish tablet",
    description: "This limestone tablet inscribed with proto-cuneiform signs is an example of Mesopotamian proto-writing.",
    image: "images/kish-tablet.png",
    imageSource: "https://commons.wikimedia.org/wiki/File:Tableta_con_trillo.png",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/Kish_tablet",
        trusted: false
      }
    ]
  },
  {
    type: "Natural Resources",
    yearRange: false,
    year: 3500,
    isEvent: true,
    event: "Start of Bronze Age in Mesopotamia.",
    references: []
  },
  {
    type: "Cooking",
    yearRange: false,
    year: 3500,
    isEvent: false,
    name: "Earliest Noted Discovery of Beer",
    description: "The earliest mention of beer in Mesopotamia is in the song 'Hymn to Ninkasi' or the goddess of brewing. Written down around 1800 BCE, the hym is no doubt much older as evidenced by the techniques detailed in the poem which scholars have determined were actually in use long before the hymn was written. Bread and beer went hand-in-hand since it is from bread dough that beer is fermented. Women were the primary brew masters, and their craft was even mentioned in Hammurabi's code.",
    image: "",
    imageSource: "", 
    references: [
      {
        //TANE pages 260 and 261 were also references
        url: "https://www.ancient.eu/article/222/the-hymn-to-ninkasi-goddess-of-beer/",
        trusted: true
      }
    ]
  },
  {
    type: "Writing",
    yearRange: true,
    yearStart: 3100,
    yearEnd: 2900,
    isEvent: false,
    name: "Jemdet Nasr tablet",
    description: "This tablet contains an administrative account of barley distribution, inscribed in proto-cuneiform. It represents a transitional stage between proto-writing and true writing.",
    image: "images/jemdet-nasr-tablet.jpg",
    imageSource: "https://www.metmuseum.org/art/collection/search/329081",
    references: [
      {
        url: "https://www.metmuseum.org/art/collection/search/329081",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: false,
    year: 3000,
    isEvent: false,
    name: "Establishment of Long-distance Trade",
    description: "A variety of goods were traded between cities such as textiles, pottery, leather goods, jewelry, spices, gains, and metals. Transportation was made easier with the invention of the wheel and sail, so items previously delivered by ox carts began being transported by water on riverboats.<br>Around 3000 BCE, Mesopotamian cities established trade all up and down the Tigris and Euphrates rivers and into Anatolia, today's Turkey. Other overland trade routes went east over the Zagros Mountains into present-day Iran and Afghanistan. By 2000 BCE, trade went in all directions. The creation of writing and math systems allowed for the organization and documentation of these economic endeavors, especially as they became more complicated and wide-spread.",
    image: "images/trade.jpeg",
    imageSource: "https://www.historyonthenet.com/wp-content/uploads/2014/09/Reconstructed_sumerian_headgear_necklaces_british_museum.JPG", 
    references: [
      {
        url: "https://www.historyonthenet.com/mesopotamian-merchants-and-traders",
        trusted: false
      }
    ]
  },
  {
    type: "Natural Resources",
    yearRange: true,
    yearStart: 2900,
    yearEnd: 2600,
    isEvent: false,
    location: "Square Temple - Eshnunna (modern Tell Asmar in Iraq)",
    name: "Stone Statue of Standing Male Worshiper",
    description: "This standing figure, with clasped hands and a wide-eyed gaze, is a worshiper. It was placed in the 'Square Temple' at Tell Asmar, perhaps dedicated to the god Abu, in order to pray perpetually on behalf of the person it represented. For humans equally were considered to be physically present in their statues. Similar statues were sometimes inscribed with the names of rulers and their families.",
    image: "images/worshiper.jpeg",
    imageSource: "https://www.metmuseum.org/art/collection/search/323735", 
    references: [
      {
        url: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/323735/711771/main-image",
        trusted: true
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2800,
    isEvent: true,
    event: "First use of the rebus system in Mesopotamian cuneiform.",
    references: []
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2600,
    isEvent: true,
    event: "First use of Mesopotamian cuneiform to write Elamite.",
    references: []
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2600,
    isEvent: true,
    event: "First use of Mesopotamian cuneiform to write Akkadian personal names.",
    references: []
  },
  {
    type: "Writing",
    yearRange: true,
    yearStart: 2600,
    yearEnd: 2500,
    isEvent: false,
    name: "Lexical list of harp strings",
    description: "This tablet contains the earliest known record of music and instruments in history.",
    image: "images/lexical-list-of-harp-strings.jpg",
    imageSource: "https://www.schoyencollection.com/music-notation/sumerian-music/earliest-music-record-ms-2340",
    references: [
      {
        url: "https://www.schoyencollection.com/music-notation/sumerian-music/earliest-music-record-ms-2340",
        trusted: true
      }
    ]
  },
  {
    type: "Natural Resources",
    yearRange: true,
    yearStart: 2600,
    yearEnd: 2500,
    isEvent: false,
    location: "Temple of Inanna/Ishtar - Nippur",
    name: "Stone Statue of Standing Female Worshiper",
    description: "This standing figure with clasped hands was buried within the temple furniture, suggesting that temple offerings and equipment remained sacred even when no longer in use.",
    image: "images/inana-worshiper.jpg",
    imageSource: "https://www.metmuseum.org/art/collection/search/329080", 
    references: [
      {
        url: "https://www.metmuseum.org/art/collection/search/329080",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: false,
    year: 2500,
    isEvent: false,
    location: "Uruk",
    name: "Rule of King Gilgamesh",
    description: "Gilgamesh was the 5th king of Uruk in Sumer. His father is said to have been Priest-King Lugalbanda and his mother Goddess Ninsun. As such, Gilgamesh was a demi-god who possessed super-human strength and lived an exceptionally long life, with records marking him to live 126 years.<br><br>He is best remembered for the Epic of Gilgamesh, which was written between 2150-1400 BCE.<br><br>His influence was so profound that myths of his divine status grew up around his deeds and finally culminated in the tales found in The Epic of Gilgamesh. Later Mesopotamian kings would invoke his name and associate his lineage with their own.",
    image: "images/gilgamesh.jpg",
    imageSource: "https://www.ancient.eu/img/r/p/500x600/2570.jpg", 
    references: [
      {
        url: "https://www.ancient.eu/gilgamesh/",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: true,
    yearStart: 2500,
    yearEnd: 612,
    isEvent: false,
    name: "Rule of Assyrian Empire",
    description: "The Assyrians ruled roughly between 2500 and 612 BCE. This was a greatly militarized society thought to be the result of its struggles with independence and expansionist police. Kings had absolute power and collected heavy taxes. Assyrian leaders were known for mass deportations of their defeated or rebellious peoples.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2400,
    isEvent: true,
    event: "First use of Mesopotamian cuneiform to write Eblaite.",
    references: []
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2350,
    isEvent: true,
    event: "First use of Mesopotamian cuneiform to write connected texts in Akkadian.",
    references: []
  },
  {
    type: "Natural Resources",
    yearRange: false,
    year: 2350,
    isEvent: false,
    location: "Assyria",
    name: "Creation of Composite Bow",
    description: "The composite bow was created by gluing together layers of wood from different trees with different elasticity and strength properies, using glues derived from animal bone and sinew. This bow design was so powerful and resilient that it persisted for centuries.",
    image: "images/08bow.jpg",
    imageSource: "http://www.mesopotamia.co.uk/warfare/explore/images/08bow.jpg",
    references: [
      {
        url: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html",
        trusted: false
      }
    ]
  },
  {
    type: "Natural Resources",
    yearRange: false,
    year: 2300,
    isEvent: false,
    name: "Creation of Seed Plow",
    description: "Sumerians revolutionized agriculture by creating the seed plow, in which a funnel was attached to the plow to deposit the seeds automatically after the plow dug the hole in the soil.",
    image: "images/plow.jpg",
    imageSource: "https://sites.google.com/a/brvgs.k12.va.us/wh-15-sem-1-mesopotamia-go/_/rsrc/1468891760247/seeder-plow/download.jpg?height=155&width=400", 
    references: [
      {
        url: "https://sciencing.com/tools-made-people-ancient-mesopotamia-9032.html",
        trusted: false
      }
    ]
  },
  {
    type: "Government",
    yearRange: true,
    yearStart: 2300,
    yearEnd: 2100,
    isEvent: false,
    name: "Rule of Akkadian Empire",
    description: "The Akkadian empire developed during the third millennia in the north of Mesopotamia (while the Sumerians were still ruling in the south). Akkadians created a new form of government where kings had an absolute power and this title was hereditary. These rulers referred to themselves as Lord of the Four Quarters (of the Earth) and eventually elevated themselves to a divine status.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2150,
    isEvent: false,
    name: "Eridu Genesis",
    description: "Eridu Genesis is the Sumerian creation myth. The first part of this text describes how the gods created the world. The second part talks about a great flood that killed all the humans, except for a handful of people who saved themselves by building an ark. This myth gives us a lot of insight into ancient Mesopotamian religion. Further, the similarities between Eridu Genesis and the corresponding myths in Abrahamic religions have important consequences for theology in general.",
    references: []
  },
  {
    type: "Writing",
    yearRange: true,
    yearStart: 2097,
    yearEnd: 2047,
    isEvent: false,
    name: "The Code of Ur-Nammu",
    description: "The Code of Ur-Nammu is the oldest known law code surviving today. Laws in the Code take the form of \"IF crime THEN punishment\" - a pattern that was followed by almost all later law codes. The Code of Ur-Nammu institutes monetary fines for less serious offenses, and capital punishment for more serious crimes.",
    image: "images/code-of-ur-nammu.jpg",
    imageSource: "https://commons.wikimedia.org/wiki/File:Ur_Nammu_code_Istanbul.jpg",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/Code_of_Ur-Nammu",
        trusted: false
      }
    ]
  },
  {
    type: "Math",
    yearRange: false,
    year: 2050,
    isEvent: true,
    event: "First use of sexagesimal place value system.",
    references: []
  },
  {
    type: "Writing",
    yearRange: false,
    year: 2000,
    isEvent: false,
    location: "Sumer",
    name: "Establishment of Scribe Schools",
    description: "During around 2000 BCE, scribal schools appeared in Ur, Nippur, and Sippar to teach students mathematics and writing, particularly as the system of cuneiform writing developed. All students learned reading, writing, math, and history, literacy, numeracy, geography, zoology, botany, astronomy, engineering, medicine, and architecture. Students would use clay tablets to practice writing; they would rewrite sentences until they were no errors.",
    image: "images/scribe-school.jpg",
    imageSource: "https://www.historyonthenet.com/wp-content/uploads/2014/09/Letter_Luenna_Louvre_AO4238.jpg", 
    references: [
      {
        url: "https://www.historyonthenet.com/mesopotamian-education-and-schools",
        trusted: false
      }
    ]
  },
  {
    type: "Civil Engineering",
    yearRange: false,
    year: 2000,
    isEvent: false,
    location: "Ur, Sumer (Present-day Nasiriyah, Iraq)",
    name: "The Great Ziggurat of Ur",
    description: "Construction began by Sumerian King Ur-Nammu during the Third Dynasty of Ur and was finished within the 21st century BCE by King Shugli. During King Shugli’s rule, the city of Ur grew to be the capital of a state controlling much of Mesopotamia, meaning that this ziggurat was an important center for Mesopotamia as a whole. During the Neo-Babylonian era, the ziggurat had deteriorated to just the base level. It was entirely rebuilt by King Nabonidus in the 6th century B.C.",
    image: "images/ziggurat.jpg",
    imageSource: "https://www.ancient.eu/image/197/great-ziggurat-of-ur/", 
    references: [
      {
        url: "https://www.historyonthenet.com/ziggurats-and-temples-in-ancient-mesopotamia",
        trusted: false
      }
    ]
  },
  {
    type: "Math",
    yearRange: false,
    year: 1800,
    isEvent: false,
    name: "Plimpton 322",
    description: "This tablet contains a list of what we would now call Pythagorean triples, written in sexagesimal.",
    image: "images/plimpton-322.jpg",
    imageSource: "https://commons.wikimedia.org/wiki/File:Plimpton_322.jpg",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/Plimpton_322",
        trusted: false
      },
      {
        url: "https://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/Robson105-120.pdf",
        trusted: true
      }
    ]
  },
  {
    type: "Math",
    yearRange: false,
    year: 1850,
    isEvent: false,
    name: "MS 2351",
    description: "This tablet contains a sexagesimal representation of 20 to the power of 20, which is a very large number.",
    image: "images/ms-2351.jpg",
    imageSource: "https://www.schoyencollection.com/mathematics-collection/arithmetics/ms-2351",
    references: [
      {
        url: "https://www.schoyencollection.com/mathematics-collection/arithmetics/ms-2351",
        trusted: true
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 1800,
    isEvent: false,
    name: "The Epic of Gilgamesh",
    description: "The Epic of Gilgamesh is one of the earliest known works of literature. The Epic follows Gilgamesh, king of Uruk, who is described as being two-thirds god and one-third man. At the start of the story, Gilgamesh is oppressing his people, and the gods create a primitive man called Enkidu who will be able to stop Gilgamesh's oppression. Gilgamesh and Enkidu fight, and Gilgamesh emerges victorious, but after the fight Enkidu and Gilgamesh become close friends. Together they go on successful adventures, in the course of which they gain some enemies among the gods. The gods eventually kill Enkidu, which devastates Gilgamesh. In response, Gilgamesh decides to go on a quest to find a secret to eternal life. On his quest he learns a lot about the world, but eventually finds out that he has no way to obtain immortality, which concludes the epic. The Epic of Gilgamesh was hugely popular in the ancient Near East and was translated into multiple languages. It continues to be a part of popular culture to this day.",
    image: "images/epic-of-gilgamesh.jpg",
    imageSource: "https://commons.wikimedia.org/wiki/File:Tablet_V_of_the_Epic_of_Gilgamesh.jpg",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/Epic_of_Gilgamesh",
        trusted: false
      },
      {
        url: "http://www.gutenberg.org/ebooks/11000",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: true,
    yearStart: 2000,
    yearEnd: 1600,
    isEvent: false,
    name: "Rule of Amorite Empire",
    description: "The Amorites ruled between roughly 2000 and 1600 BCE. They adopted the tyrannical form of government that was established by the Akkadians, and they established the first Babylonian Dynasty. With accomplishments like Hammurabi’s Code, they maintained a centralized administration and created legislative and jurisdictional authority",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://anciv.info/mesopotamia/form-of-government-in-mesopotamia.html",
        trusted: false
      }
    ]
  },
  {
    type: "Math",
    yearRange: true,
    yearStart: 1800,
    yearEnd: 1600,
    isEvent: false,
    name: "YBC 7289",
    description: "This tablet contains an extremely accurate approximation of the square root of two, written in sexagesimal.",
    image: "images/ybc-7289.jpg",
    imageSource: "https://commons.wikimedia.org/wiki/File:YBC-7289-OBV-labeled.jpg",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/YBC_7289",
        trusted: false
      },
      {
        url: "https://www.maa.org/press/periodicals/convergence/the-best-known-old-babylonian-tablet",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: true,
    yearStart: 1792,
    yearEnd: 1750,
    isEvent: false,
    location: "Babylon",
    name: "Rule of King Hammurabi",
    description: "Hammurabi was the sixth king of the Amorite First Dynasty of Babylon, following the throne from his father Sin-Muballit. He was known for expanding the kingdom of Babylon with the intentions of conquering all of Mesopotamia. At that time of his rise to power, Kingdom of Babylon consisted of the cities of Babylon, Kish, Sippar, and Borsippa. By 1750 BCE, he had control of the entire region of Babylon.<br><br>He was also know for providing for his people. Hammurabi was referred to as 'bani matim', or 'builder of the land' for commissioning the construction of buildings and canals to develop the region. He also established efforts for food distribtuion, land and arhchitectural beautification, and legal systems.<br><br>Finally, he is most remembered for his code of 282 laws. They were written with the goal of pleasing the gods. They were very strict and absolute and provided a number of crimes and their applicable punishents, one of the most notable examples being 'an eye for an eye'. The laws were carved into stone to emphasize their permanence and were displayed publicly so that no citizen or slave could claim he was not aware of a certain law or infraction.",
    image: "images/541.jpg",
    imageSource: "https://www.ancient.eu/article/68/hammurabis-code-babylonian-law-set-in-stone/", 
    references: [
      {
        url: "https://www.ancient.eu/hammurabi/",
        trusted: true
      }
    ]
  },
  {
    type: "Writing",
    yearRange: false,
    year: 1790,
    isEvent: false,
    name: "The Code of Hammurabi",
    description: "The Code of Hammurabi is one of the most important law codes in the ancient world. The Code contains 282 laws, making it the longest Old Babylonian text. Punishments in the code are graded based on the social status of the victim and the perpetrator, but in many cases the laws follow the \"eye for an eye\" principle. Laws in the Code encompass a variety of areas: contracts, minimum wages, slavery, family, religion, military service, and more. The Code of Hammurabi also contains one of earliest examples of presumption of innocence.",
    image: "images/code-of-hammurabi.jpeg",
    imageSource: "https://commons.wikimedia.org/wiki/File:P1050763_Louvre_code_Hammurabi_face_rwk.JPG",
    references: [
      {
        url: "https://en.wikipedia.org/wiki/Code_of_Hammurabi",
        trusted: false
      },
      {
        url: "http://www.general-intelligence.com/library/hr.pdf",
        trusted: true
      }
    ]
  },
  {
    type: "Cooking",
    yearRange: false,
    year: 1700,
    isEvent: false,
    name: "Cooling and Refrigeration First Utilized",
    description: "The ruler Zimri-lin of Mari built ice houses, 'bit shuripim', near his capital.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://books.google.com/books?id=A3xqDwAAQBAJ&lpg=PP1&pg=PT473#v=onepage&q&f=false",
        trusted: true
      }
    ]
  },
  {
    type: "Natural Resources",
    yearRange: false,
    year: 1500,
    isEvent: true,
    event: "Appearance of non-meteoritic, smelted iron objects in Mesopotamia.",
    references: []
  },
  {
    type: "Natural Resources",
    yearRange: true,
    yearStart: 1243,
    yearEndyo: 1207,
    isEvent: false,
    name: "Inscribed Tablet",
    description: "This tablet commemorates this restoration of the temple of the Goddess Ishtar in the capital city of Assur as well as notes the good deeds and conquests of King Tukulti-Ninurta I. The inscription concludes with a curse on anyone would would attempt to remove the king's name from the tablet.",
    image: "images/assur-stone.jpg",
    imageSource: "https://www.themorgan.org/sites/default/files/images/exhibitions/written-in-stone.jpg", 
    references: [
      {
        url: "https://www.themorgan.org/exhibitions/written-in-stone",
        trusted: true
      }
    ]
  },
  {
    type: "Civil Engineering",
    yearRange: false,
    year: 900,
    isEvent: false,
    name: "Use of Wells",
    description: "By 900 BCE, wood was used in the building of wells. As wells were created to retrieve water, some wells were made large and deep. While the steps were usually made from stone or the bedrock from the soil, the walls would sometimes be lined with wood (or stone or plaster) to hold it up. Water would be retrieved by a bucket and rope, somtimes accompanied with a wooden crank.",
    image: "",
    imageSource: "", 
    references: [
      {
       url: "https://books.google.com/books?id=A3xqDwAAQBAJ&lpg=PP1&pg=PT473#v=onepage&q&f=false",
        trusted: true
      }
    ]
  },
  {
    type: "Civil Engineering",
    yearRange: false,
    year: 800,
    isEvent: false,
    location: "Assyria",
    name: "Canal Systems Constructed",
    description: "The first sophisticated long-distance canal systems were constructed in the Assyrian Empire. The purpose of aqueducts were “to transport water from one place to another, achieving a regular and controlled water supply to a place that would not otherwise have received sufficient water to meet basic needs such as irrigation of food crops and drinking fountains. Being able to control water allowed agriculture to flourish and allowed cities to expand to beyond just along water sources.",
    image: "",
    imageSource: "", 
    references: [
      {
        url: "https://www.ancient.eu/aqueduct/",
        trusted: true
      }
    ]
  },
  {
    type: "Government",
    yearRange: true,
    yearStart: 668,
    yearEnd: 627,
    isEvent: false,
    location: "Assyria",
    name: "Rule of King Ashurbanipal",
    description: "Ashurbanipal was the last of the great kings of Assyria, living from 668-627 BCE. He was the son of King Esarhaddon of the Neo-Assyrian Empire.<br>He is known for achieving the greatest territorial expansion of the Assyrian Empire, eventually capturing Babylonia, Persia, Syria, and briefly Egypt. He was popular among his citizens, but known to be cruel to those whom he defeated.<br>He is also known for his grand library in Nineveh, which housed over 30,000 clay tablets, including the Babylonian Epic of Creation and the Epic of Gilgamesh.",
    image: "images/138.jpg",
    imageSource: "https://www.ancient.eu/img/r/p/500x600/138.jpg", 
    references: [
      {
        url: "https://www.ancient.eu/Ashurbanipal/",
        trusted: true
      }
    ]
  },
];

const RULERS = [
  /*{
    name: "Hammurabi",
    yearStart: 1792,
    yearEnd: 1750
  }*/
];
